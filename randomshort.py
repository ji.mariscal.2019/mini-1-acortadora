import webapp
import random
import urllib.parse

form = """
    <form name="form" action="" method="POST">
    <p>URL: <input type="text" name="content"></p>
    <input type="submit" value="Send"></form>
"""

class Randomshort(webapp.webApp):
    urls = {}

    def parse(self, received):
        try:
            recv = received.decode()
        except UnicodeDecodeError as e:
            print(f"Error decoding request: {e}")
            return None, None, None

        parts = recv.split(' ')
        method = parts[0]
        resource = parts[1]
        body = recv.split('\r\n\r\n')[1] if method == "POST" else None
        return method, resource, body

    def handle_get(self, resource):
        if resource == "/":
            http = "200 OK"
            html = ("<html><body>" + "URL LIST: " + str(self.urls) + "\n""<br>" + form + "</body></html>")
        elif resource in self.urls:
            http = "200 OK"
            html = "<html><body> Resource: " + resource + "..." \
                   + '<meta http-equiv="refresh" content="2;url=' + (self.urls[resource]) + \
                   '">' "</body></html></body></html>"
        else:
            http = "404 Not Found"
            html = "Not found"
        return http, html

    def handle_post(self, body):
        url = body.split('content=')[1]
        url = urllib.parse.unquote_plus(url)
        print(url)
        rand = random.randint(1, 100000)
        short = "/" + str(rand)
        short_url = "http://localhost:8080" + short
        print(short)
        print(short_url)

        if url == "":
            http = "404 Not Found"
            html = "No URL provided"
        else:
            if not (url.startswith('https://') or url.startswith('http://')):
                url = "https://" + url

            self.urls[short] = url
            http = "200 OK"
            html = f"<html><body>Original URL: <a href='{url}'>{url}</a>" \
                   f"<br><br>Short URL: <a href='{short_url}'>{short_url}</a></body></html>"
        return http, html

    def process(self, analyzed):
        method, resource, body = analyzed
        if method == "GET":
            return self.handle_get(resource)
        elif method == "POST":
            return self.handle_post(body)
        else:
            return "404 Not Found", "Not Found"

    def __init__(self, hostname, port):
        super().__init__(hostname, port)


if __name__ == "__main__":
    testWebApp = Randomshort("localhost", 8080)
